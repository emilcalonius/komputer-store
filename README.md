# The Komputer Store
A web application for buying computers.

## Table of contents
* [Background](#introduction)
* [Usage](#usage)
* [Authors](#authors)
* [Sources](#sources)

## Background
The goal of this project was to create a dynamic webpage using "vanilla" JavaScript. 
The website is designed to be responsive and to work well on many different sizes of screens.

The application aims to demonstrate skills with JavaScript, HTML and CSS.

## Usage
Clone repository and host application locally.

## Authors
[Emil Calonius](https://gitlab.com/emilcalonius)

## Sources
Project was an assignment done during education program created by
Noroff Education
