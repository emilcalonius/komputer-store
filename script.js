let loan = 0;
let balance = 0;
let pay = 0;

// Get dropdown menu elements
const dropdown = document.querySelector(".dropdown");
const select = dropdown.querySelector(".select");
const caret = dropdown.querySelector(".caret");
const menu = dropdown.querySelector(".menu");
const selected = dropdown.querySelector(".selected");

// Get elements displaying computer info
const features = document.querySelector(".features");
const image = document.querySelector(".image");
const computerName = document.querySelector(".computer-name");
const computerInfo = document.querySelector(".computer-info");
const price = document.querySelector(".price");

// Get the buttons
const loanBtn = document.querySelector(".get-loan");
const transferBtn = document.querySelector(".transfer");
const workBtn = document.querySelector(".do-work");
const buyBtn = document.querySelector(".buy");
const payLoanBtn = document.querySelector(".pay-loan");

// Add event listener to handle menu opening and closing
select.addEventListener("click", () => {
    select.classList.toggle("select-clicked");
    caret.classList.toggle("caret-rotate");
    menu.classList.toggle("menu-open");
});

// Add event listener for getting money through working
workBtn.addEventListener("click", () => {
    // Add 100 Kr. to pay
    let payElem = document.querySelector(".pay-amount");
    let currentPay = parseInt(payElem.innerText.split(" ")[0]);
    let newPay = currentPay + 100;
    pay = newPay;
    payElem.innerText = newPay + " Kr.";
});

// Add event listener for transferring money to bank
transferBtn.addEventListener("click", () => {
    if(loan === 0) {
        balance += pay;
    } else {
        // 10% of pay goes to repaying loan
        // transfer 90% of pay to bank
        // If there is money left after paying loan transfer that also to bank
        let forLoan = pay * 0.1;
        balance += pay * 0.9;
        if(forLoan <= loan) {
            loan -= forLoan;
        } else {
            forLoan -= loan;
            loan = 0;
            balance += forLoan; 
        }
    }
    // Update balance on page
    document.querySelector(".balance-amount").innerText = balance + " Kr.";
    // If loan is 0, remove it from the page
    if(loan === 0) {
        document.querySelector(".pay-loan").classList.remove("visible");
        document.querySelector(".loan").classList.remove("loan-active");
    } else {
        document.querySelector(".loan-amount").innerText = loan + " Kr.";
    }
    // Reset pay to 0
    pay = 0;
    document.querySelector(".pay-amount").innerText = "0 Kr.";
});

// Add event listener for getting a loan
loanBtn.addEventListener("click", () => {
    if(loan !== 0) {
        alert("Cannot get another loan!");
        return;
    }
    let loanAmount = parseInt(prompt("Enter loan amount:"));
    if(loanAmount > 2 * balance) {
        alert("Loan too large!");
        return;
    }
    if(loanAmount <= 0 || isNaN(loanAmount)) {
        alert("Invalid loan!");
        return;
    }
    loan = loanAmount;
    document.querySelector(".loan-amount").innerText = loan + " Kr.";
    document.querySelector(".pay-loan").classList.add("visible");
    document.querySelector(".loan").classList.add("loan-active");

    // Update balance
    balance += loanAmount;
    document.querySelector(".balance-amount").innerText = balance + " Kr.";
});

// Add event listener for repaying loan
payLoanBtn.addEventListener("click", () => {
    if(loan > pay) {
        loan -= pay;
    } else if(loan <= pay) {
        pay -= loan;
        loan = 0;
        balance += pay; 
        document.querySelector(".balance-amount").innerText = balance + " Kr.";
    }
    // If loan is 0, remove it from the page
    if(loan === 0) {
        document.querySelector(".pay-loan").classList.remove("visible");
        document.querySelector(".loan").classList.remove("loan-active");
    } else {
        document.querySelector(".loan-amount").innerText = loan + " Kr.";
    }
    pay = 0;
    document.querySelector(".pay-amount").innerText = "0 Kr.";
});

// Add event listener for buying a computer
buyBtn.addEventListener("click", () => {
    let price = parseInt(document.querySelector(".price").textContent);
    // Alert if not enough balance
    if(price > balance) {
        alert("Not enough money!");
        return;
    }
    // Update balance
    balance -= price;
    document.querySelector(".balance-amount").innerText = balance + " Kr.";
    // Alert saying laptop has been bought
    alert("You are now the owner of the new laptop!");
});

function fillDropDown() {
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        .then((response) => response.json())
        .then((computers) => {
            // Update content on site to match first computer on the list
            features.innerText = computers[0].specs.join(" \n");
            image.src = "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image;
            computerName.innerText = computers[0].title;
            computerInfo.innerText = computers[0].description;
            price.innerText = computers[0].price + " NOK";

            // Create a list item element for each computer in the API
            computers.forEach((computer, index) => {
                const option = document.createElement("li");
                option.id = computer.id;
                option.innerText = computer.title;
                if(index === 0) selected.innerText = option.innerText;

                // Add an event listener that handles the dropdown menu closing
                // and changing the information on the page
                option.addEventListener("click", () => {
                    // Handle dropdown
                    selected.innerText = option.innerText;
                    select.classList.remove("select-clicked");
                    caret.classList.remove("caret-rotate");
                    menu.classList.remove("menu-open");

                    // Update computer info shown
                    features.innerText = computer.specs.join(" \n");
                    image.src = "https://noroff-komputer-store-api.herokuapp.com/" + computer.image;
                    computerName.innerText = computer.title;
                    computerInfo.innerText = computer.description;
                    price.innerText = computer.price + " NOK";
                });
                menu.append(option);
            });
        })
        .catch((error) => console.log(error));
}

fillDropDown();